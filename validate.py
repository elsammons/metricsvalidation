#!/usr/bin/env python3
from jira.client import JIRA
from optparse import OptionParser
from queries import select_queries
import os
import sys

jira_server = 'https://issues.jboss.org'
target_release = ""


if __name__ == '__main__':
    #
    # Parse command line options
    #
    try:
        jira_user = os.environ['JIRA_USER']
        jira_pass = os.environ['JIRA_PASS']
    except KeyError:
        jira_user = ''
        jira_pass = ''

    get_rates = ('regression_count', 'resolution_count',
                 'reopen_count', 'rejected_count',
                 'verification_count', 'failed_qe_count')

    op = OptionParser()
    op.add_option('', '--project', dest='project',
                  help='The project you wish to verify')
    op.add_option('-d', '--date', dest='q_date',
                  help='Sets the start date to query, yyyy-mm-dd,yyyy-mm-dd')
    op.add_option('-t', '--target_release', dest='target_release',
                  help='Set the scope of the query to a given target release')
    op.add_option('-u', '--user', dest='username', default=jira_user,
                  help='JIRA Username')
    op.add_option('-p', '--password', dest='password', default=jira_pass,
                  help='JIRA Password')
    op.add_option('-q', '--queries', dest='queries_only', default=False,
                  action='store_true',
                  help='Print out the queries used to evaluate.')
    op.add_option('', '--csv', dest='csv', default=False,
                    action='store_true', help='Set to provide csv formatting in STDOUT')

    opts, args = op.parse_args()

    if opts.target_release:
        target_release = 'and "Target Release" = "' + opts.target_release + '"'

    ## TODO: Fix Version

    jira_auth = (opts.username, opts.password)
    #
    # Setup JIRA connection
    #
    options = {
        'server': jira_server,
        'verify': True
    }

    jira = JIRA(options, basic_auth=jira_auth)
    project = opts.project.upper()
    queries = project

    s_date, e_date = opts.q_date.split(',')

    total_bugs_query = (
        'project = {} and type = bug and '
        'created <= {} {}'
    ).format(project, e_date, target_release)

    queries = select_queries(project, target_release, s_date, e_date)

    if opts.queries_only:
        print('Total bugs: %s\n' % total_bugs_query)
        for query in queries:
            print('%s : %s\n' % (query, queries[query]))
        sys.exit(0)

    get_rates = ('regression_count', 'resolution_count',
                 'reopen_count', 'rejected_count',
                 'verification_count', 'failed_qe_count')

    total_bugs = jira.search_issues(total_bugs_query,
                                    startAt=0, maxResults=0, json_result=True)['total']

    ready_for_qa_query = (
        'project = {} and type = bug '
        'and status in ("Ready for QA") '
        'and resolution in ("Done", "Unresolved")'
    ).format(project)

    total_ready_for_qa = jira.search_issues(ready_for_qa_query,
                                            startAt=0, maxResults=0,
                                            json_result=True)['total']

    if target_release != "":
        text, target_release = target_release.split('=')

    print('\nProject: %s target release:%s start date: %s end date %s' %
          (project, target_release, s_date, e_date))

    print('TOTAL Bugs used for denominator: %d' % total_bugs)
    print('TOTAL Bugs used for denominator in Verification Rate: %d' % total_ready_for_qa)
    for query in queries:
        results = jira.search_issues(queries[query],
                                     startAt=0, maxResults=0, json_result=True)['total']
        if opts.csv:
            print('%s,%d' %(query, results))
        else:
            print('\t%s: %d' % (query, results))
            
        if query in get_rates:
            if query == "verification_count":
                try:
                    rate = (results / total_ready_for_qa) * 100
                except ZeroDivisionError as zde:
                    print("\t%s Rate can NOT divide by zero" % query)
                    continue
            else:
                rate = (results / total_bugs) * 100
            if opts.csv:
                print('%s rate,%.2f%%' % (query, rate))
            else:
                print('\t%s rate: %.2f%%' % (query, rate))
    print('\n')
